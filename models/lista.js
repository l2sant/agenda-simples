const { Schema, model } =  require('mongoose')


const listaSchema = Schema({
    nome: {type: String, required: true},
    numero: {type: Number, required: true}
})

listaSchema.index({
    nome: 'text'
})


module.exports = model('Lista', listaSchema)