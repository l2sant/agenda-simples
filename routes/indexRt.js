const express = require('express')
const route = express.Router()

const { index,
        pesquisar,
        adicionar,
        modForm,
        modificar,
        deletar } = require('../controllers/lista')

route.get('/', index)
route.get('/pesq', pesquisar)
route.post('/salvar', adicionar)
route.get('/modForm/:id', modForm)
route.post('/modificar/:id', modificar)
route.get('/del/:id', deletar)



module.exports = route