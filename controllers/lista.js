const Lista = require('../models/lista')


const index = async(req, res) => {
    let lista  = await Lista.find({})
    res.render('index', {lista})
}


const pesquisar = async(req, res) => {
    const termo = new RegExp(`^${req.query.termo}`, 'i')
    const lista = await Lista.find({
        nome: {$regex: termo}
    })
    res.render('index', {lista})
}


const adicionar = async(req, res) => {
    const lista = await Lista({
        nome: req.body.nome,
        numero: req.body.numero
    })
    lista.save()
         .then(() => {
            res.redirect('/')
         })
}


const deletar = async(req, res) => {
    await Lista.deleteOne({_id: req.params.id})
               .then(() => res.redirect('back'))
               .catch(err =>  console.log(err))
}


const modForm = async(req, res) => {
    await Lista.findById({_id: req.params.id})
               .then((item) => {
                   res.render('modForm', {item})
               })
               .catch(err =>  console.log(err.name))
}


const modificar = async(req, res) => {
    const { nome, numero } = req.body
    await Lista.updateOne({_id: req.params.id}, {nome: nome, numero: numero})
               .then(() => res.redirect('/'))       
}


module.exports = {
    deletar,
    modificar,
    modForm,
    adicionar,
    pesquisar,
    index,

}