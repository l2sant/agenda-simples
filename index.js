const express = require('express')
const app = express()
const mongoose = require('mongoose')

const port = process.env.PORT || 3000
const mongo = process.env.MONGODB || 'mongodb://localhost:27017/agenda'

const indexRt = require('./routes/indexRt')

app.set('views')
app.set('view engine', 'ejs')

app.use(express.urlencoded({extended: true}))

app.use('/', indexRt)


mongoose
    .connect(mongo, { useNewUrlParser: true })
    .then(() => {
        app.listen(port, () => console.log("server on!"))
    })
